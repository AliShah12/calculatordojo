package com.aka.dojo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class CalculatorTest {

    @Test
    void addEmptyStrings(){
        Calculator calculator = new Calculator();
        assertEquals(0, calculator.add(""));
    }
}
